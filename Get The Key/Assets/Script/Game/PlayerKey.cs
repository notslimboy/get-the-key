﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKey : MonoBehaviour
{
    public const int Key = 0;

    public int currentKey = Key;
    public bool isKey = true;
    private bool isLocalPlayer;

    void Start()
    {
        PlayerController pc = GetComponent<PlayerController>();
        isLocalPlayer = pc.isLocalPlayer;
    }

    public void TakeKey(GameObject player, int amount)
    {
        currentKey += amount;
        NetworkManager n = NetworkManager.instance.GetComponent<NetworkManager>();
        n.CommandKeyChange(player,amount,isKey);
    }
}
