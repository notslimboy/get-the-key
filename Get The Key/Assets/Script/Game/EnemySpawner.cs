/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject spawnPoint;
    public int numberOfEnemies;
    [HideInInspector]
    public List<SpawnPoint> enemySpawnPoints;
    private void Start()
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            var spawnPosition = new Vector2(Random.Range(-8f, 8f), 0f);
            SpawnPoint enemySpawnPoint = (Instantiate(spawnPoint, spawnPosition, Quaternion.identity) as GameObject).GetComponent<SpawnPoint>();


            enemySpawnPoints.Add(enemySpawnPoint);
        }

        // SpawnEnemies();
    }


    public void SpawnEnemies(NetworkManager.EnemiesJSON enemiesJSON) 
    {
        foreach(NetworkManager.userJSON enemyJSON in enemiesJSON.enemies)
        {
            Vector2 position = new Vector2(enemyJSON.position[0], enemyJSON.position[1]);
            GameObject newEnemy = Instantiate(enemy, position, Quaternion.identity) as GameObject;
            newEnemy.name = enemyJSON.name;
            PlayerController pc = newEnemy.GetComponent<PlayerController>();
            pc.isLocalPlayer = false;
            PlayerHealth h = newEnemy.GetComponent<PlayerHealth>();
            h.currentHealth = enemyJSON.health;
            h.OnChangeHealth();
            h.isEnemy = true;
            h.destroyOnDeath = true;
        }
    }

}