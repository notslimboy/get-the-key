﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fire : MonoBehaviour
{
    [SerializeField] int decreaseHealth;

    [SerializeField] GameObject handlerObject;

    GameHandler handler;

    void Start()
    {
        decreaseHealth = 10;
        handler = handlerObject.GetComponent<GameHandler>();
        
    }

    void Update()
    {
        StartCoroutine(ShowAndHide(this.gameObject, 1.0f)); // 1 second
        limitHealth();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            //Decrese Health
            handler.decreaseHealth(decreaseHealth);
            handler.setHealthPlayer();
        }
    }

    IEnumerator ShowAndHide(GameObject go, float delay)
    {
        go.SetActive(true);
        yield return new WaitForSeconds(delay);
        go.SetActive(false);
    }

    public void limitHealth()
    {
        if(handler.healthTotal == 0)
        {
            decreaseHealth = 0;
        }
    }


}
