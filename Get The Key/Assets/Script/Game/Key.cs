﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key : MonoBehaviour
{
    public int key;
    [SerializeField] GameObject handlerObject;

    public GameObject player;
    GameHandler handler;

    void Start()
    {
        key = 1;
        handler = handlerObject.GetComponent<GameHandler>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            // * Add Key
            handler.addKey(key);
            Destroy(this.gameObject);
            PlayerKey pk = player.GetComponent<PlayerKey>();
            pk.TakeKey(player,1);
        }
    }

}
