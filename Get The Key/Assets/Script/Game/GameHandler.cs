﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    [SerializeField] GameObject keyText;

    [SerializeField] GameObject healthText;
    public GameObject player;
    private int keyTotal;
    Fire fire;
    public int healthTotal = 100;


    private void Update()
    {
        loseCondition();
        winCondition();
    }   
    public void setKeyText(string text)
    {
        keyText.GetComponent<Text>().text = text;
    }

    public void setHealthText(string text)
    {
        healthText.GetComponent<Text>().text = text;
    }

    public void addKey(int value)
    {
        //Add Key
        keyTotal += value;
        
        setKeyText($"Key: {keyTotal}");
    }

    public void decreaseHealth(int value)
    {
        //Decrease Health
        healthTotal -= value;
        setHealthText($"Health: {healthTotal}");
    }

    public void setHealthPlayer()
    {
        if (healthTotal <= 0)
        {
            healthTotal = 0;
        }
    }

    public void winCondition()
    {
        if(keyTotal == 4)
        {
            SceneManager.LoadScene("gameOverScene");
        }
    }

    public void loseCondition()
    {
        if (healthTotal == 0)
        {
            SceneManager.LoadScene("gameOverScene");
        }

    }
}
