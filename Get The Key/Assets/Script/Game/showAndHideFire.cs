﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showAndHideFire : MonoBehaviour
{
    public GameObject[] fire;

    // Start is called before the first frame update
    void Start()
    {
        fire = GameObject.FindGameObjectsWithTag("Fire");
        StartCoroutine("Hide");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Hide()
    {


        foreach (GameObject go in fire)
        {
            go.SetActive(false);
        }
        yield return new WaitForSecondsRealtime(1);
        foreach (GameObject go in fire)
        {
            go.SetActive(true);
        }
        yield return new WaitForSecondsRealtime(1);
        StartCoroutine("Hide");
    }
}
