﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using SocketIO;
using Project.Utility;

namespace Project.Network
{
    public class NetworkClient : SocketIOComponent
    {
        [Header("Network Client")]
        [SerializeField]
        private Transform networkContainer;
        [SerializeField]
        private GameObject playerPrefab;

        public static string clientID { get; private set; }

        private Dictionary<string, NetworkIdentity> serverObjects;

        public override void Start()
        {
            base.Start();
            setupEvent();
        }

        public override void Update()
        {
            base.Update();
        }

        private void initialized()
        {
            serverObjects = new Dictionary<string, NetworkIdentity>();
        }

        private void setupEvent()
        {
            On("open", (E) =>
             {
                 Debug.Log("connection made with server");
             });

            On("register", (E) =>
            {
                clientID = E.data["id"].ToString().RemoveQuotes();
                Debug.LogFormat("Our Client's id({0})", clientID);
            });

            On("spawn", (E) =>
             {
                 //Handling all spawning players
                 //Passed Data
                 string id = E.data["id"].ToString().RemoveQuotes();

                 GameObject go = Instantiate(playerPrefab, networkContainer);
                 go.name = string.Format("Player ({0})", id);
                 NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                 ni.SetControllerID(id);
                 ni.SetSocketReference(this);
                 serverObjects.Add(id, ni);
                 Debug.Log(id);
             });

            On("disconnect", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();

                GameObject go = serverObjects[id].gameObject;
                Destroy(go); // remove object 
                serverObjects.Remove(id);

            }
            );

            On("updatePosition", (E) =>
            {
                string id = E.data["id"].ToString().RemoveQuotes();
                float x = E.data["position"]["x"].f;
                float y = E.data["position"]["y"].f;

                NetworkIdentity ni = serverObjects[id];
                ni.transform.position = new Vector3(x, y, 0);
                Debug.Log(x);
                Debug.Log(y);
            }
            );

            On("updateKey", (E) =>
                {
                    float key = E.data["key"].f;
                    Debug.Log(key);

                }
                );

        }
    }

    [Serializable]
    public class Player
    {
        public string id;
        public Position position;
    }

    [Serializable]
    public class Position
    {
        public float x;
        public float y;
    }
}

