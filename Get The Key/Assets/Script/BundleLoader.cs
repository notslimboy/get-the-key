﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO.Compression;


public class BundleLoader : MonoBehaviour
{
    public string url;
    public string PlayerUrl;
    public bool isDone = false;

    public GameObject DownloadButton;

    public GameObject PlayButton;

    public GameObject OkButton;

    public void Download()
    {
        StartCoroutine(GetGameBundle());
    }

    public void DownloadAgain()
    {
        StartCoroutine(GetPlayerBundle());
    }
    public void Play()
    {
        SceneManager.LoadScene("gameScene");
    }

    IEnumerator GetGameBundle()
    {
        WWW www = new WWW(url);

        yield return www;

        AssetBundle bundle = www.assetBundle;
        if(www.error == null)
        {
            DownloadButton.SetActive(false);
            PlayButton.SetActive(true);
            OkButton.SetActive(false);
        }
        else
        {
            Debug.Log(www.error);
        }

        isDone = true;
    }


    IEnumerator GetPlayerBundle()
    {
        WWW www = new WWW(PlayerUrl);

        yield return www;

        AssetBundle bundle = www.assetBundle;
        if (www.error == null)
        {
            PlayButton.SetActive(false);
            OkButton.SetActive(true);
        }
        else
        {
            Debug.Log(www.error);
        }

        isDone = true;
    }
    
}
