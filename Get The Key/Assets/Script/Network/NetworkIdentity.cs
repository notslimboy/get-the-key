﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using Project.Utility.Attributes;
using UnityEngine;
using SocketIO;

namespace Project.Network
{
    public class NetworkIdentity : MonoBehaviour
    {
        [Header("Helpful values")]
        [SerializeField]
        [GreyOut]
        private string id;
        [SerializeField]
        [GreyOut]
        private bool isControlling;
        private SocketIO.SocketIOComponent socket;

        // Start is called before the first frame update
        public void Awake()
        {
            isControlling = false;

        }

        // Update is called once per frame
        public void SetControllerID(string ID)
        {
            id = ID;
            isControlling = (NetworkClient.clientID == ID) ? true : false; // check incoming ID versues that one we have save in server 

        }

        public void SetSocketReference(SocketIOComponent Socket)
        {
            socket = Socket;
        }

        public string GetID()
        {
            return id;
        }

        public bool IsControlling()
        {
            return isControlling;
        }

        public SocketIOComponent GetSocket()
        {
            return socket;
        }
    }

}
