﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float runSpeed = 20.0f;
    public bool isLocalPlayer = false;
    Vector2 oldPosition;
    Vector2 currentPosition;

    // Start is called before the first frame update
    void Start()
    {
        oldPosition = transform.position;
        currentPosition = oldPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * runSpeed;
        var y = Input.GetAxis("Vertical") * Time.deltaTime * runSpeed;

        transform.Translate(x, y, 0);

        currentPosition = transform.position;

        if(currentPosition != oldPosition)
        {
            NetworkManager.instance.GetComponent<NetworkManager>().CommandMove(transform.position);
            oldPosition = currentPosition;
        }

    }
}
