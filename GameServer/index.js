/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */
const express = require('express');
const path = require('path');
const app = express();


var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static(path.join(__dirname, 'public')));


// * Port
server.listen(3000);

// global variables for the server
var enemies = [];
var playerSpawnPoints = [];
var clients = [];

app.get('/', function (req, res) {
    res.send('hey you got back get "/"');
});

io.on('connection', function (socket) {

    var currentPlayer = {};
    currentPlayer.name = 'unknown';

    socket.on('player connect', function () {
        console.log(currentPlayer.name + ' recv: player connect');
        for (var i = 0; i < clients.length; i++) {
            var playerConnected = {
                name: clients[i].name,
                position: clients[i].position,
                rotation: clients[i].position,
                health: clients[i].health,
                key: clients[i].key
            };
            // in your current game, we need to tell you about the other players.
            socket.emit('other player connected', playerConnected);
            console.log(currentPlayer.name + ' emit: other player connected: ' + JSON.stringify(playerConnected));
        }
    });

    socket.on('play', function (data) {
        console.log(currentPlayer.name + ' recv: play: ' + JSON.stringify(data));
        // if this is the first person to join the game init the enemies
        if (clients.length === 0) {
            numberOfEnemies = data.enemySpawnPoints.length;
            enemies = [];
            data.enemySpawnPoints.forEach(function (enemySpawnPoint) {
                var enemy = {
                    name: guid(),
                    position: enemySpawnPoint.position,
                    rotation: enemySpawnPoint.rotation,
                    health: 100
                };
                enemies.push(enemy);
            });
            playerSpawnPoints = [];
            data.playerSpawnPoints.forEach(function (_playerSpawnPoint) {
                var playerSpawnPoint = {
                    position: _playerSpawnPoint.position,
                    rotation: _playerSpawnPoint.rotation
                };
                playerSpawnPoints.push(playerSpawnPoint);
            });
        }

        var enemiesResponse = {
            enemies: enemies
        };
        // we always will send the enemies when the player joins
        console.log(currentPlayer.name + ' emit: enemies: ' + JSON.stringify(enemiesResponse));
        socket.emit('enemies', enemiesResponse);
        var randomSpawnPoint = playerSpawnPoints[Math.floor(Math.random() * playerSpawnPoints.length)];
        currentPlayer = {
            name: data.name,
            position: randomSpawnPoint.position,
            rotation: randomSpawnPoint.rotation,
            health: 100,
            key: 0
        };
        clients.push(currentPlayer);
        // in your current game, tell you that you have joined
        console.log(currentPlayer.name + ' emit: play: ' + JSON.stringify(currentPlayer));
        socket.emit('play', currentPlayer);
        // in your current game, we need to tell the other players about you.
        socket.broadcast.emit('other player connected', currentPlayer);
    });

    socket.on('player move', function (data) {
        console.log('recv: move: ' + JSON.stringify(data));
        currentPlayer.position = data.position;
        socket.broadcast.emit('player move', currentPlayer);
    });


    socket.on('health', function (data) {
        console.log(currentPlayer.name + ' recv: health: ' + JSON.stringify(data));
    
        if (data.from === currentPlayer.name) {
            var indexDamaged = 0;
            if (!data.isEnemy) {
                clients = clients.map(function (client, index) {
                    if (client.name === data.name) {
                        indexDamaged = index;
                        client.health -= data.healthChange;
                    }
                    return client;
                });
            } else {
                enemies = enemies.map(function (enemy, index) {
                    if (enemy.name === data.name) {
                        indexDamaged = index;
                        enemy.health -= data.healthChange;
                    }
                    return enemy;
                });
            }

            var response = {
                name: (!data.isEnemy) ? clients[indexDamaged].name : enemies[indexDamaged].name,
                health: (!data.isEnemy) ? clients[indexDamaged].health : enemies[indexDamaged].health
            };
            console.log(currentPlayer.name + ' bcst: health: ' + JSON.stringify(response));
            socket.emit('health', response);
            socket.broadcast.emit('health', response);
        }
    });

    socket.on('key', function (data) {
        console.log(currentPlayer.name + ' recv: key: ' + JSON.stringify(data));

        if (data.from === currentPlayer.name) {
            var indexKey = 0;
            if (data.isKey) {
                clients = clients.map(function (client, index) {
                    if (client.name === data.name) {
                        indexKey = index;
                        client.key += data.keyChange;
                    }
                    return client;
             a   });
            } 

            var response = {
                name: (!data.isEnemy) ? clients[indexKey].name : enemies[indexKey].name,
                key: (data.isKey) ? clients[indexKey].key : enemies[indexKey].health
            };
            console.log(currentPlayer.name + ' bcst: key: ' + JSON.stringify(response));
            socket.emit('key', response);
            socket.broadcast.emit('key', response);
        }
    });

    socket.on('disconnect', function () {
        console.log(currentPlayer.name + ' recv: disconnect ' + currentPlayer.name);
        socket.broadcast.emit('other player disconnected', currentPlayer);
        console.log(currentPlayer.name + ' bcst: other player disconnected ' + JSON.stringify(currentPlayer));
        for (var i = 0; i < clients.length; i++) {
            if (clients[i].name === currentPlayer.name) {
                clients.splice(i, 1);
            }
        }
    });

});

console.log('--- server is running ...');

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}