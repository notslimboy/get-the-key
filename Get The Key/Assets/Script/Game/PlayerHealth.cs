﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    [SerializeField] const int maxHealth = 100;
    public bool destroyOnDeath = false;
    public bool isEnemy = false;
    public int currentHealth = maxHealth;
    private bool _isLocalPlayer;
    public RectTransform healthBar;
    // Start is called before the first frame update
    void Start()
    {
        PlayerController pc = GetComponent<PlayerController>();
        _isLocalPlayer = pc.isLocalPlayer;
    }

    public void TakeDamage(GameObject playerFrom, int amount)
    {
        currentHealth -= amount;

    }

    public void OnChangeHealth()
    {
        healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
        {
            if (currentHealth <= 0)
            {
                if (destroyOnDeath)
                {
                    Destroy(gameObject);
                }
                else
                {
                    currentHealth = maxHealth;
                    healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
                    Respawn();
                }
            }
        }
    }

    private void Respawn()
    {
        if(_isLocalPlayer)
        {
            Vector2 spawnPoint = Vector2.zero;
            transform.position = spawnPoint;
        }
    }
}
